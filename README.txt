FIFO Lock
===========

Previously, you most likely to use $lock = \Drupal::lock(); 

REF:https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Lock%21LockBackendInterface.php/group/lock/8.9.x

With this module all you would need to change is

change from
$lock = \Drupal::lock();
to
$lock = \Drupal::service('fifo_lock');

-- Example code start --
  public function long_operation_example()
  {
    $lockName = 'test_lock';
    //$drupalLock = \Drupal::lock();
    $drupalLock = \Drupal::service('fifo_lock');

    $this->acquireLockWithWait($drupalLock, $lockName);

    if ($drupalLock->acquire($lockName)) {
      // Do the long operation here.
      //sleep(10);

      $database = \Drupal::database();
      $query = $database->query("DO SLEEP(1);");
      $result = $query->fetchAll();

      $drupalLock->release($lockName);
    }
  }

  public function acquireLockWithWait($drupalLock, $lockName, $timeout = 30, $numOfConcurrentThread = 3)
  {
    //Add a random delay for extra safe, as drupal lock is not perfect due to database connection latency
    $randomInt = mt_rand(1, 111);
    usleep(3003 * $randomInt); //1000000 = 1sec

    $maxLoop = $numOfConcurrentThread + 1;
    $loopCount = 0;

    try {
      while (!$drupalLock->acquire($lockName, $timeout) && $loopCount < $maxLoop) {
        $drupalLock->wait($lockName, $timeout); //maximum wait seconds
        $loopCount++;
      }
    } catch (\Drupal\Core\Database\DatabaseExceptionWrapper $e) {
      return false;
    } catch (\Exception $e) {
      return false;
    }

    return true;
  }
-- Example code end --
